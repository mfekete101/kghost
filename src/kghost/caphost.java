package kghost;

import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataInputStream;
import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Calendar;
import javax.imageio.ImageIO;

public class caphost extends Thread {

    String savepath;
    
    public caphost(String spcall) {
        savepath = spcall;
    }

    @Override
    public void run() {
        try {
            //listen for clients on port 6970
            ServerSocket serverSocket = new ServerSocket(6970);
            //this thread will accept client connections
            new capreceive(serverSocket, savepath).start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class capreceive extends Thread {

    ServerSocket serverSocket;
    String savepath;

    public capreceive(ServerSocket serverSocketcall, String spcall) {
        savepath = spcall;
        serverSocket = serverSocketcall;
    }

    @Override
    public void run() {
        try {
            Socket socket = serverSocket.accept();
            new capreceive(serverSocket, savepath).start();
            DataInputStream inputStream = new DataInputStream(socket.getInputStream());
            //read incoming message length
            String connector = inputStream.readUTF().replaceAll("[\\\\/:*?\"<>|]", "").replaceAll("\n", "").replaceAll("\r", "");
            int length = inputStream.readInt();
            if (length > 0) {
                //read message with a length already given
                byte[] screenshot = new byte[length];
                inputStream.readFully(screenshot, 0, screenshot.length);
                BufferedImage image = caputil.base64StringToImg(new String(screenshot));
                //save image to path
                String ms = System.currentTimeMillis() + "";
                ms = ms.substring(ms.length() - 3);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime()) + "." + ms;
                ImageIO.write(image, "png", new File(savepath + connector + "." + timeStamp + ".png"));
                System.out.println("Received " + image.getWidth() + "x" + image.getHeight() + ": " + System.currentTimeMillis());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

class caputil {

    public static String imgToBase64String(final RenderedImage img, final String formatName) {
        //convert image to base64 format (necessary for client)
        final ByteArrayOutputStream os = new ByteArrayOutputStream();
        try {
            ImageIO.write(img, formatName, Base64.getEncoder().wrap(os));
            return os.toString(StandardCharsets.ISO_8859_1.name());
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }

    public static BufferedImage base64StringToImg(final String base64String) {
        //convert base64 format to bufferedimage (necessary for host)
        try {
            return ImageIO.read(new ByteArrayInputStream(Base64.getDecoder().decode(base64String)));
        } catch (final IOException ioe) {
            throw new UncheckedIOException(ioe);
        }
    }
}
